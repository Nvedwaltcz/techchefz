import React from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";
import ClientLogo from "../../assets/images/drive.png";
import ClientLogo2 from "../../assets/images/fugiat.png";

// STYLE
import "../../assets/scss/slick/index.scss";
import "../../assets/scss/components/clients-carousel/index.scss";

export default function index(props) {
  const clientsCarouselSettings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true
  };
  return (
    <div className="position-relative">
      <Row>
        <Slider
          {...clientsCarouselSettings}
          className="clients-carousel position-relative col-12"
        >
          <div className="py-5">
            <Row>
              {props.data.homePage.clientsCarousel.map((item, index) => (
                <Col md="6" key={index}>
                  <img
                    src={item.url}
                    className="img-fluid my-4 mx-auto"
                    alt=""
                  />
                </Col>
              ))}
            </Row>
          </div>
          <div className="py-5">
            <Row>
              {props.data.homePage.clientsCarousel1.map((item, index) => (
                <Col md="6" key={index}>
                  <img
                    src={item.url}
                    className="img-fluid my-4 mx-auto"
                    alt=""
                  />
                </Col>
              ))}
            </Row>
          </div>
        </Slider>
      </Row>
    </div>
  );
}
