import React from "react";
import Slider from "react-slick";
import Avatar from "../../assets/images/avatar.jpg";

// STYLE
import "../../assets/scss/components/testimonial-carousel/index.scss";

export default function index(props) {
  const TestimonialCarouselSettings = {
    dots: false,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <div>
      <Slider
        {...TestimonialCarouselSettings}
        className="testimonial-carousel d-flex flex-wrap align-items-md-center position-relative col-md-12"
      >
        {props.data.homePage.testimonial.map((item, index) => (
          <div className="testi-box border-right px-5" key={index}>
            <img src={item.url} className="img-fluid rounded-circle" alt="" />
            <p className="my-4">{item.description}</p>
            <div className="pt-4">
              <strong>{item.title}</strong>
              <p>{item.designation}</p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
