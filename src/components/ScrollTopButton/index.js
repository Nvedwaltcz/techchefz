import React, { useState } from "react";
import { Button } from "reactstrap";

export default function ScrollToTop() {
  const [intervalId, setIntervalId] = useState(0);

  const scrollStep = () => {
    if (window.pageYOffset === 0) {
      clearInterval(intervalId);
    }
    window.scroll(0, window.pageYOffset - "50");
  };

  const scrollToTop = () => {
	 
    let intervalId = setInterval(scrollStep, "16.66");
    setIntervalId({ intervalId: intervalId });
  };

  return (
    <Button
      color="primary"
      className="btn-primary scroll-more-btn animation--from-right-fade rounded-0 p-0"
      onClick={scrollToTop}
    >
      <span className="more-text d-block position-relative text-capitalize">
        scroll to top
      </span>
      <span class="bar-wrapper bar-wrapper--down p-event-none">
        <span class="bar-whitespace p-event-none">
          <span class="bar-line bar-line--down p-event-none"></span>
        </span>
      </span>
    </Button>
  );
}
