import React, { useState } from "react";
import {} from "reactstrap";
import { Link } from "react-router-dom";

// style
import "../../assets/scss/components/link-button/index.scss";
export default function linkButton() {
  return (
    <div>
      <Link
        to="#"
        className="button-block w-100 text-left position-relative border-none pl-0 d-block"
      >
        <h4 class="m-0 text-capitalize">Frontend Developer </h4>
        <span class="button_bottom_line"></span>
      </Link>
      <Link
        to="#"
        className="button-block w-100 text-left position-relative border-none pl-0 d-block"
      >
        <h4 class="m-0 text-capitalize">Java Developer </h4>
        <span class="button_bottom_line"></span>
      </Link>
      <Link
        to="#"
        className="button-block w-100 text-left position-relative border-none pl-0 d-block"
      >
        <h4 class="m-0 text-capitalize">Digital Project manager </h4>
        <span class="button_bottom_line"></span>
      </Link>
    </div>
  );
}
