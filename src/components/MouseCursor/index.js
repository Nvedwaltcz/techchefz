import React, { useRef, useEffect, useState } from 'react';
import { TweenMax } from "gsap";
import $ from "jquery"

export default function Index() {
    const cursor = useRef();
    const cursorFollower = useRef();

    let [posX, setPosX] = useState(0)
    let [posY, setPosY] = useState(0);
    let [mouseX, setmouseX] = useState(0);
    let [mouseY, setmouseY] = useState(0);

    useEffect(() => {
        $(document).mousemove(e => {
            setmouseX(e.pageX);
            setmouseY(e.pageY);
        });
        TweenMax.to({}, 0.016, {
            repeat: -1,
            onRepeat: function () {
                // posX += (mouseX - posX) / 9;
                // posY += (mouseY - posY) / 9;
                setPosX(prePosX => ((mouseX - prePosX) / 9) + prePosX);
                setPosY(prePosY => ((mouseY - prePosY) / 9) + prePosY);

                TweenMax.set(cursorFollower.current, {
                    css: {
                        left: posX - 12,
                        top: posY - 12
                    }
                });

                TweenMax.set(cursor.current, {
                    css: {
                        left: mouseX,
                        top: mouseY
                    }
                });
            }
        });
    }, [cursor, cursorFollower, posY, posX, mouseX, mouseY]);

    return (
        <div>
            <div className="cursor" ref={cursor}></div>
            <div className="cursor-follower" ref={cursorFollower}></div>
        </div>
    )
}
