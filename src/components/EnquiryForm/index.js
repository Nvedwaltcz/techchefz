import React from 'react';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';

// STYLE
import '../../assets/scss/components/enquiry-form/index.scss'

export default function index() {
    return (
        <div className="enquiry-form-container position-relative">
            <Container className="bg-white px-5 py-5">
                <Row className="justify-content-between">
                    <Col md="6">
                        <div className="title-block">
                            <h5 className="title-decoration title-decoration-left text-uppercase position-relative mb-4">get in touch</h5>
                            <h2 className="text-uppercase mb-4">interested work with us?</h2>
                            <p>If it dosen’t challenge you it does’nt change you.</p>
                        </div>
                    </Col>
                    <Col md="5" className="d-flex align-items-center">
                        <Form className="w-100">
                            <FormGroup>
                                <Label for="emailId" hidden>Email</Label>
                                <Input className="rounded-0" type="email" name="email" id="emailId" placeholder="Email" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="productType" hidden>Email</Label>
                                <Input className="rounded-0" type="select" name="select" id="productType">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="message" hidden>Email</Label>
                                <Input className="rounded-0" type="textarea" name="text" id="message" rows="4" />
                            </FormGroup>
                            <Button color="primary" className="rounded-0" block>Submit</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
