import React from "react";
import { Card, CardBody, CardText } from "reactstrap";
import Masonry from "react-masonry-css";

// STYLE
import "../../assets/scss/components/mansory-grids/index.scss";
import picThumb from "../../assets/images/photo.png";

export default function index(props) {
  const breakpointColumnsObj = {
    default: 2,
    1100: 3,
    700: 2,
    500: 1
  };
  return (
    <div className="mansory-grids-layout">
      <Masonry
        breakpointCols={breakpointColumnsObj}
        className="my-masonry-grid"
        columnClassName="my-masonry-grid_column"
      >
        {props.data.homePage.masonry.map((item, index) => (
          <Card key={index}>
            <CardBody className="p-5">
              <img src={item.url} className="mb-3" alt="Strategy" />
              <h3 className="card-title text-capitalize mb-3">{item.title}</h3>
              <CardText>{item.description}</CardText>
            </CardBody>
          </Card>
        ))}
      </Masonry>
    </div>
  );
}
