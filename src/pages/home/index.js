import React, { useRef } from "react";
import { Col, Container, Row, Button } from "reactstrap";
import Data from "../../data.json";

// COMPONENTS
import HeroCarousel from "../../components/HeroCarousel";
import MansoryGrids from "../../components/MansoryGrids";
import ClientsCarousel from "../../components/ClientsCarousel";
import TestimonialCarousel from "../../components/TestimonialCarousel";
import EnquiryForm from "../../components/EnquiryForm";

import ClientBg from "../../assets/images/client-bg.jpg";

export default function Index() {
  const whoWeAre = useRef();

  const scrollToNext = () =>
    whoWeAre.current.scrollIntoView({
      behavior: "smooth",
      block: "start",
      offset: "150"
    });
  return (
    <div>
      <section className="home-section-wrapper position-relative">
        <Container fluid className="px-5">
          <Row className="justify-content-between">
            <Col md="4" className="d-flex align-items-center">
              <Row>
                <Col md="8">
                  <div className="title-block">
                    <h5 className="title-decoration title-decoration-left text-uppercase position-relative mb-4">
                      we are
                    </h5>
                    <h2 className="text-uppercase mb-4">what we do</h2>
                    <p>If it dosen’t challenge you it does’nt change you.</p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md="7">
              <HeroCarousel />
            </Col>
          </Row>
        </Container>
        <Button
          color="primary"
          className="btn-primary scroll-more-btn animation--from-left-fade rounded-0 p-0"
          onClick={scrollToNext}
        >
          <span className="more-text d-block position-relative text-capitalize">
            more
          </span>
          <span class="bar-wrapper bar-wrapper--down p-event-none">
            <span class="bar-whitespace p-event-none">
              <span class="bar-line bar-line--down p-event-none"></span>
            </span>
          </span>
        </Button>
      </section>
      <section
        className="home-section-wrapper py-5 d-flex flex-wrap align-items-center"
        ref={whoWeAre}
      >
        <Container fluid className="px-5">
          <Row className="justify-content-between">
            <Col md="6">
              <MansoryGrids data={Data} />
            </Col>
            <Col md="5" className="d-flex align-items-center">
              <Row>
                <Col md="8">
                  <div className="title-block">
                    <h5 className="title-decoration title-decoration-left text-uppercase position-relative mb-4">
                      we are
                    </h5>
                    <h2 className="text-uppercase mb-4">techchefz</h2>
                    <p>If it dosen’t challenge you it does’nt change you.</p>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="home-section-wrapper position-relative py-5">
        <Container fluid className="pl-5">
          <Row className="timeline-brand-wrapper-container align-items-center">
            <Col md="7" className="timeline-brand-wrapper">
              <Row>
                <Col
                  md="7"
                  className="d-flex align-items-center position-relative bg-white"
                >
                  <Row>
                    <Col md="11">
                      <div className="title-block">
                        <h5 className="title-decoration title-decoration-left text-uppercase position-relative mb-4">
                          we are
                        </h5>
                        <h2 className="text-uppercase mb-4">brands</h2>
                        <p>
                          If it dosen’t challenge you it does’nt change you. If
                          it dosen’t challenge you it does’nt change you. If it
                          dosen’t challenge you it does’nt change you.
                        </p>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col md="5" className="px-0">
                  <ClientsCarousel data={Data} />
                </Col>
              </Row>
            </Col>
          </Row>
          <Col md="8" className="client-bg-thumb position-absolute px-0">
            <img src={ClientBg} alt="" />
          </Col>
        </Container>
      </section>
      <section className="home-section-wrapper d-flex align-items-center flex-wrap py-5">
        <Container fluid className="pr-5">
          <Row className="justify-content-between">
            <Col md="8">
              <TestimonialCarousel data={Data} />
            </Col>
            <Col md="3" className="d-flex align-items-center">
              <div className="title-block">
                <h5 className="title-decoration title-decoration-left text-uppercase position-relative mb-4">
                  we are
                </h5>
                <h2 className="text-uppercase mb-4">testimonials</h2>
                <p>If it dosen’t challenge you it does’nt change you.</p>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <EnquiryForm />
    </div>
  );
}
