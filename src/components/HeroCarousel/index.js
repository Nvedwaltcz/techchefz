import React, { useState } from 'react';
import Slider from "react-slick";
import { Link } from 'react-router-dom';

// STYLE
import '../../assets/scss/components/hero-carousel/index.scss'

export default function Index() {
    const [nav1, setNav1] = useState(null)
    const [nav2, setNav2] = useState(null)
    const heroCarouselSettings = {
        dots: false,
        infinite: true,
        arrows: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    };
    const heroNextCarouselSettings = {
        dots: false,
        infinite: true,
        arrows: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    };
    return (
        <div className="hero-carousel position-relative">
            <div className="hero-big-carousel position-relative">
                <Slider {...heroCarouselSettings} asNavFor={nav2} ref={slider1 => setNav1(slider1)}>
                    <div className="position-relative">
                        <picture>
                            <source media="(max-width: 767px)" srcSet="https://www.playground.it/assets/img/hp-slider-tnf-cmyk-w1024@1x.jpg" />
                            <img src="https://www.playground.it/assets/img/hp-slider-tnf-cmyk-w1024@1x.jpg" alt="" />
                        </picture>
                        <div className="position-absolute hero-big-caption">
                            <span className="text-white text-uppercase mb-3 d-inline-block position-relative title-decoration title-decoration-left">latest Projects</span>
                            <h2 className="text-white">Royal Enfield</h2>
                            <h3 className="text-white mb-4">The Global Motorcycle Brand</h3>
                            <Link to="/" className="text-white py-3 text-decoration-none position-relative title-decoration title-decoration-right">Explore</Link>
                        </div>
                    </div>
                    <div className="position-relative">
                        <picture>
                            <source media="(max-width: 767px)" srcSet="https://www.playground.it/assets/img/hp-slider-napapijri-zeknit-w1024@1x.jpg" />
                            <img src="https://www.playground.it/assets/img/hp-slider-napapijri-zeknit-w1024@1x.jpg" alt="" />
                        </picture>
                        <div className="position-absolute hero-big-caption">
                            <span className="text-white text-uppercase mb-3 d-inline-block position-relative title-decoration title-decoration-left">latest Projects</span>
                            <h2 className="text-white">Royal Enfield</h2>
                            <h3 className="text-white mb-4">The Global Motorcycle Brand</h3>
                            <Link to="/" className="text-white py-3 text-decoration-none position-relative title-decoration title-decoration-right">Explore</Link>
                        </div>
                    </div>
                </Slider>
            </div>
            <div className="hero-next-carousel position-absolute">
                <Slider {...heroNextCarouselSettings} asNavFor={nav1} ref={slider2 => setNav2(slider2)}>
                    <div className="position-relative">
                        <picture>
                            <source media="(max-width: 767px)" srcSet="https://www.playground.it/assets/img/hp-slider-napapijri-zeknit-w1024@1x.jpg" />
                            <img src="https://www.playground.it/assets/img/hp-slider-napapijri-zeknit-w1024@1x.jpg" alt="" />
                        </picture>
                        <div className="position-absolute hero-big-caption">
                            <h2 className="text-white">Royal Enfield</h2>
                        </div>
                    </div>
                    <div className="position-relative">
                        <picture>
                            <source media="(max-width: 767px)" srcSet="https://www.playground.it/assets/img/hp-slider-tnf-cmyk-w1024@1x.jpg" />
                            <img src="https://www.playground.it/assets/img/hp-slider-tnf-cmyk-w1024@1x.jpg" alt="" />
                        </picture>
                        <div className="position-absolute hero-big-caption">
                            <h2 className="text-white">Royal Enfield</h2>
                        </div>
                    </div>
                </Slider>
            </div>
            <div className="header-slide-status position-absolute">

            </div>
        </div>
    )
}