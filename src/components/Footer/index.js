import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import ScrollTopButton from "../ScrollTopButton";
// STYLE
import "../../assets/scss/components/footer/index.scss";

export default function index() {
  return (
    <footer className="footer bg-dark position-relative">
      {/* <Button color="primary" className="btn-primary scroll-more-btn animation--from-right-fade rounded-0 p-0">
                <span className="more-text d-block position-relative text-capitalize">scroll to top</span>
                <span class="bar-wrapper bar-wrapper--down p-event-none">
                    <span class="bar-whitespace p-event-none">
                        <span class="bar-line bar-line--down p-event-none"></span>
                    </span>
                </span>
            </Button> */}
      <ScrollTopButton />
      <div className="footer-social-list">
        <ul className="social-icons navbar-nav d-flex flex-column">
          <li>
            <a href="#" className="d-block">
              <svg id="ico-in" viewBox="0 0 25 24">
                <path d="M5.65 24V7.807H.315V24H5.65zM2.983 5.595c1.86 0 3.018-1.243 3.018-2.797C5.967 1.209 4.843 0 3.018 0 1.193 0 0 1.209 0 2.798c0 1.554 1.158 2.797 2.948 2.797h.035zM8.603 24h5.335v-9.043c0-.484.034-.967.175-1.313.386-.967 1.264-1.969 2.737-1.969 1.931 0 2.703 1.485 2.703 3.662V24h5.335v-9.285c0-4.974-2.632-7.288-6.142-7.288-2.878 0-4.142 1.623-4.844 2.728h.036V7.807H8.603c.07 1.519 0 16.193 0 16.193z"></path>
              </svg>
            </a>
          </li>
          <li>
            <a href="#" className="d-block">
              <svg id="ico-fb" viewBox="0 0 11 24">
                <path d="M2.358 4.528v3.218H0v3.936h2.358v11.695h4.844V11.682h3.25s.304-1.887.452-3.951H7.22v-2.69c0-.403.528-.944 1.05-.944h2.639V0H7.321C2.238 0 2.358 3.939 2.358 4.528z"></path>
              </svg>
            </a>
          </li>
          <li>
            <a href="#" className="d-block">
              <svg id="ico-ig" viewBox="0 0 24 24">
                <path d="M23.928 7.053a8.82 8.82 0 0 0-.557-2.913 5.881 5.881 0 0 0-1.386-2.125A5.881 5.881 0 0 0 19.86.629a8.81 8.81 0 0 0-2.912-.557C15.667.013 15.259 0 12 0 8.741 0 8.333.013 7.053.072A8.82 8.82 0 0 0 4.14.629a5.897 5.897 0 0 0-2.127 1.386A5.879 5.879 0 0 0 .628 4.14a8.81 8.81 0 0 0-.557 2.912C.013 8.333 0 8.741 0 12c0 3.259.013 3.667.072 4.947.02.995.208 1.98.557 2.913a5.881 5.881 0 0 0 1.386 2.125 5.881 5.881 0 0 0 2.125 1.386 8.81 8.81 0 0 0 2.912.557C8.333 23.987 8.741 24 12 24c3.259 0 3.667-.013 4.947-.072a8.82 8.82 0 0 0 2.913-.557 6.138 6.138 0 0 0 3.511-3.511 8.81 8.81 0 0 0 .557-2.912C23.987 15.667 24 15.259 24 12c0-3.259-.013-3.667-.072-4.947zm-2.16 9.796a6.639 6.639 0 0 1-.413 2.227 3.977 3.977 0 0 1-2.278 2.279 6.645 6.645 0 0 1-2.228.413c-1.266.057-1.645.069-4.849.069s-3.584-.012-4.849-.069a6.639 6.639 0 0 1-2.227-.413 3.706 3.706 0 0 1-1.381-.898 3.715 3.715 0 0 1-.898-1.38 6.645 6.645 0 0 1-.413-2.228c-.057-1.266-.069-1.645-.069-4.849s.012-3.584.069-4.849a6.639 6.639 0 0 1 .413-2.227c.192-.522.499-.994.898-1.381a3.715 3.715 0 0 1 1.38-.898 6.645 6.645 0 0 1 2.228-.413c1.266-.057 1.645-.069 4.849-.069s3.584.012 4.849.069a6.639 6.639 0 0 1 2.227.413c.522.192.994.499 1.381.898.399.387.705.858.898 1.38.264.713.404 1.467.413 2.228.057 1.266.069 1.645.069 4.849s-.012 3.584-.069 4.849zM12 5.837a6.163 6.163 0 1 0 0 12.325 6.163 6.163 0 0 0 0-12.325zM12 16a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm7.845-10.405a1.44 1.44 0 1 1-2.88 0 1.44 1.44 0 0 1 2.88 0z"></path>
              </svg>
            </a>
          </li>
        </ul>
      </div>
      <Container className="px-5">
        <Row className="flex-row-reverse">
          <Col md="3">
            <p class="text-white">
              A - 53, 1st Floor, FIEE Complex Okhla Industrial Area Phase 2 New
              Delhi - 110020, Delhi, India
            </p>
            <p>
              <a
                href="tel:+919711829204"
                class="d-block text-white text-decoration-none"
              >
                <strong class="mr-4">T</strong>+91 97118 29204
              </a>
            </p>
            <p>
              <a
                href="mailto:info@techchefz.com"
                class="d-block text-white text-decoration-none"
              >
                <strong class="mr-4">M</strong>info@techchefz.com
              </a>
            </p>
          </Col>
          <Col md="3">
            <p class="text-white">
              A - 53, 1st Floor, FIEE Complex Okhla Industrial Area Phase 2 New
              Delhi - 110020, Delhi, India
            </p>
            <p>
              <a
                href="tel:+919711829204"
                class="d-block text-white text-decoration-none"
              >
                <strong class="mr-4">T</strong>+91 97118 29204
              </a>
            </p>
            <p>
              <a
                href="mailto:info@techchefz.com"
                class="d-block text-white text-decoration-none"
              >
                <strong class="mr-4">M</strong>info@techchefz.com
              </a>
            </p>
          </Col>
        </Row>
      </Container>
      <div className="footer-bottom-row">
        <Container fluid>
          <Row>
            <Col md="6" className="d-flex align-items-center px-5">
              <p className="text-uppercase mb-0 text-white">
                &copy; 2020 All rights reserved
              </p>
            </Col>
            <Col md="6" className="navbar bg-primary py-0 px-5">
              <ul className="navbar-nav d-flex flex-row">
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    who we are
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    what we do
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    case studies
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    careers
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    href="#"
                    className="text-white nav-link py-4 px-3 text-uppercase"
                  >
                    contact us
                  </a>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
      </div>
    </footer>
  );
}
