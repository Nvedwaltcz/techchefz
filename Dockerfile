# base image
FROM node

#Create App Directory
RUN mkdir -p /usr/src/app/

# set working directory
WORKDIR /usr/src/app/

# add `/app/node_modules/.bin` to $PATH
ENV DEBUG $DEBUG

# install and cache app dependencies
COPY . .

RUN npm install

ADD src /usr/src/app/src
ADD public /usr/src/app/public

# start app
CMD ["npm", "start"]